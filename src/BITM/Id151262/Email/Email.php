<?php

namespace App\Email;

use App\Database as DB;

use PDO;
use App\Message\Message;
use App\Utility\Utility;
class Email extends DB
{

    public $id = "";

    public $name = "";

    public $email = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('email',$data)){
            $this->email = $data['email'];
        }
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->email);
        $STH = $DBH->prepare("INSERT INTO `email`(`id`, `name`, `email`) VALUES (NULL ,?,?)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Email: $this->email ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}