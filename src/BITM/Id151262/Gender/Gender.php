<?php

namespace App\Gender;

use App\Database as DB;

use PDO;
use App\Message\Message;
use App\Utility\Utility;
class Gender extends DB
{

    public $id = "";

    public $name = "";

    public $gender = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name= $data['name'];
        }
        if(array_key_exists('gender',$data)){
            $this->gender = $data['gender'];
        }

    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->gender);
        $STH = $DBH->prepare("INSERT INTO `gender`(`id`, `name`, `gender`) VALUES (NULL ,?,?)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ name: $this->name ] , [ Gender: $this->gender ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}