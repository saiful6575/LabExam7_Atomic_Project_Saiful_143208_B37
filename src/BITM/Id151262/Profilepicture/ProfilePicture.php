<?php

namespace App\ProfilePicture;

use App\Database as DB;

use PDO;
use App\Message\Message;
use App\Utility\Utility;
class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $picture;

    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('file',$data)){
            $this->picture = $data['file'];
        }
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->picture);
        $STH = $DBH->prepare("INSERT INTO `profile_picture`(`id`, `name`, `picture`) VALUES (NULL ,?,?)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Picture: $this->picture] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}// end of BookTitle class