<?php
require_once("../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add City</title>
    <link rel="stylesheet" href="../../resource/css/style.css">

    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
</head>
<body>
<h2>Create City</h2>
<form class="form-horizontal" method="post" action="store.php">
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Name:</label>
        <div class="col-sm-4">
            <input type="text" name="username" class="form-control" id="username" placeholder="Enter Name" >
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">City:</label>
        <div class="col-sm-4">
            <input type="text" name="city" class="form-control" id="city" placeholder="Enter City" >
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="submit" class="btn btn-info">Create</button>
        </div>
    </div>
</form>
</body>
</html>

