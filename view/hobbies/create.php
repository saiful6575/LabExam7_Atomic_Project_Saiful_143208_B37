<?php
require_once("../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add hobbies</title>
    <link rel="stylesheet" href="../../resource/css/style.css">

    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
</head>
<body>
<h2>Create hobbies</h2>
<form class="form-horizontal" method="post" action="store.php">
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">name:</label>
        <div class="col-sm-4">
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" size="10px">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">select hobbies:</label>
        <div class="col-sm-4">
            <input type="checkbox" name="hobbies[]" value="programming">Programming<br>
            <input type="checkbox" name="hobbies[]" value="football playing">Football playing<br>
            <input type="checkbox" name="hobbies[]" value="dancing">Dancing<br>
            <input type="checkbox" name="hobbies[]" value="singing">singing<br>
            <input type="checkbox" name="hobbies[]" value="drawing">Drawing<br>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="submit" class="btn btn-info">Create</button>
        </div>
    </div>
</form>
</body>
</html>
